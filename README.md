# .emacs

My emacs config. Used everywhere.

`nix build` results in an emacs installation with all required packages included.

## Prerequisites

```console
$ nix --version
nix (Nix) 2.18.8
```

## Installation

```console
$ git clone https://codeberg.org/mian/emacs-config.git ~/.emacs.d
$ cd ~/.emacs.d
$ nix build
$ ./result/bin/emacs
```

## Organization

| Location | Description |
| --- | --- |
| /init.el | Top-level entrypoint. Read this first. |
| /lisp/ | Topic-specific config modules. |
| /lisp/languages/ | Programming language configs. |

## License: [Unlicense](./UNLICENSE)

This is free and unencumbered software released into the public domain.
