{ pkgs, ... }:
pkgs.emacsWithPackagesFromUsePackage {
  package = if pkgs.stdenv.hostPlatform.isLinux then pkgs.emacs30-pgtk else pkgs.emacs30;
  config = "";
  alwaysEnsure = true;
  override = epkgs: epkgs // {
    telega = epkgs.melpaPackages.telega;
  };
  extraEmacsPackages = epkgs: with epkgs; [
    ace-window
    adaptive-wrap
    aggressive-indent
    all-the-icons
    biome
    burly
    cmake-mode
    consult
    consult-project-extra
    corfu
    crystal-mode
    daemons
    delight
    denote
    diff-hl
    direnv
    dogears
    doom-themes
    drag-stuff
    dslide
    eat
    elfeed
    elfeed-org
    # elfeed-tube
    embark
    embark-consult
    ement
    eslint-fix
    fennel-mode
    fontaine
    fullframe
    git-link
    git-timemachine
    haskell-mode
    helpful
    hide-mode-line
    highlight-indent-guides
    htmlize
    journalctl-mode
    js2-mode
    json-mode
    keycast
    ledger-mode
    lua-mode
    magit
    marginalia
    markdown-mode
    moc
    mood-one-theme
    moody
    nerd-icons
    nim-mode
    nix-mode
    no-littering
    orderless
    org
    org-attach-screenshot
    org-modern
    pdf-tools
    popper
    ppp
    qrencode
    quickrun
    rainbow-delimiters
    ron-mode
    rust-mode
    shackle
    sideline
    sideline-flymake
    smartparens
    sudo-edit
    telega
    tempel
    templatel
    terraform-mode
    tide
    tldr
    toml-mode
    typescript-mode
    vertico
    visual-fill-column
    web-mode
    weblorg
    webpaste
    whitespace-cleanup-mode
    writeroom-mode
    yaml-mode
    zoom

    pkgs.mu
  ];
}
