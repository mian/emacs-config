;;; -*- lexical-binding: t -*-

(let ((prev-gc-threshold gc-cons-threshold)
      (prev-gc-percentage gc-cons-percentage))
  ;; Defer GC as much as possible during init
  (setq gc-cons-threshold (* 1 1000 1000 1000)
        gc-cons-percentage 0.8)
  ;; Resume normal GC well after init quiets down
  (add-hook
   'after-init-hook
   (lambda ()
     (run-with-idle-timer
      10 ;; sec
      nil
      (lambda ()
        (setq gc-cons-threshold (* 100 prev-gc-threshold)
              gc-cons-percentage prev-gc-percentage))))))

(setq-default package-enable-at-startup nil
              package-archives nil
              package-load-list nil
              byte-compile-warnings nil
              warning-suppress-types '((comp))
              comp-async-report-warnings-errors nil
              native-comp-async-report-warnings-errors nil
              load-prefer-newer t
              frame-inhibit-implied-resize t
              bidi-inhibit-bpa t
              bidi-display-reordering 'left-to-right
              bidi-paragraph-direction 'left-to-right
              inhibit-compacting-font-caches t
              read-process-output-max (* 1024 1024))

;; scroll-bar-mode on Sway doesn't affect the first frame with setq. Requires setopt for hook or direct fn call.
(setopt menu-bar-mode nil
        scroll-bar-mode nil
        tool-bar-mode nil)

;; Prefix match to deal with unpredictable darwin behavior
(defun my/system-member-p (systems)
  (not (not (seq-find (lambda (h) (string-prefix-p h (system-name))) systems))))

(defvar my/default-text-height (if (my/system-member-p '("bocchi")) 280 200))
(set-face-attribute 'default nil :height my/default-text-height)
