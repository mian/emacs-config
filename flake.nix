{
  description = "emacs stuff";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    flake-utils.url = "github:numtide/flake-utils";
    emacs-overlay = {
      url = "github:nix-community/emacs-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nixpkgs-stable.follows = "nixpkgs";
    };
    tdlib = {
      url = "github:tdlib/td";
      # url = "github:tdlib/td?rev=44b548c3075818af24409f561bd0390a84d3a727";
      flake = false;
    };
  };
  outputs = inputs@{ nixpkgs, flake-utils, emacs-overlay, ... }:
    flake-utils.lib.eachDefaultSystem (system: let
      tdOverlay = (self: super: {
        tdlib = super.tdlib.overrideAttrs(old: {
          version = "unstable";
          src = inputs.tdlib;
        });
      });
      pkgs = import nixpkgs {
        inherit system;
        overlays = [ tdOverlay emacs-overlay.overlay ];
      };
    in {
      packages.default = import ./default.nix { inherit pkgs; };
    });
}
