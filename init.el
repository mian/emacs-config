;;; -*- lexical-binding: t -*-

;; Bootstrap
(eval-and-compile
  (add-to-list 'load-path (expand-file-name "lisp/" user-emacs-directory))
  (add-to-list 'load-path (expand-file-name "lisp/languages/" user-emacs-directory))
  (add-to-list 'load-path (expand-file-name "site-lisp/" user-emacs-directory))
  (let ((default-directory (expand-file-name "site-lisp/" user-emacs-directory)))
    (normal-top-level-add-subdirs-to-load-path)))

(require 'init-startup-profile)

;; Pre-theme
(set-background-color "#212337")

(require 'init-packages)
(require 'init-exec-path)

(use-package no-littering
  :demand t
  :preface
  (declare-function no-littering-expand-etc-file-name nil)
  :config
  (setq-default custom-file (no-littering-expand-etc-file-name "custom.el")))

(use-package dash :demand t)

;; Visuals
(require 'init-theme)
(require 'init-ui)

;; Tools
(require 'init-ace)
(require 'init-git)
(require 'init-completion)
(require 'init-org)
(require 'init-popups)
(require 'init-projects)
(require 'init-tramp)

;; Coding
(require 'init-edit-setup)
(require 'init-prog-setup)
(require 'init-csharp)
(require 'init-elisp)
(require 'init-fennel)
(require 'init-html)
(require 'init-javascript)
(require 'init-markdown)
(require 'init-lua)
(require 'init-nix)
(require 'init-python)
(require 'init-rust)
(require 'init-terminal)
(require 'init-terraform)
(require 'init-typescript)
(require 'init-yaml)

;; Other clients
(when (my/system-member-p '("bocchi" "maho" "rin"))
  (require 'init-feeds))
(require 'init-matrix)
(require 'init-telegram)
(require 'init-mail)
(require 'init-weather)
