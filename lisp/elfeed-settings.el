;;; -*- lexical-binding: t; -*-

(require 'cl-lib)
(require 'dash)
(require 'f)
(require 's)

(require 'elfeed)
(require 'elfeed-search)
(require 'elfeed-show)
(require 'elfeed-utils)
(require 'youtube-dl)

;; Add video tag to YT feeds
(defadvice elfeed (after configure-elfeed activate)
  (add-hook 'elfeed-new-entry-hook
            (elfeed-make-tagger :feed-url "youtube\\.com"
                                :add 'video)))

;; Fix feed titles
(defadvice elfeed-search-update (before configure-elfeed-search-update activate)
  (let ((feed (elfeed-db-get-feed "http://www.theonion.com/feeds/rss")))
    (setf (elfeed-feed-title feed) "The Onion")))

(defun my/elfeed-fix-title (entry)
  (interactive)
  (let* ((title (elfeed-entry-title entry))
         (do-all-fixes (-compose
                        #'my/title-case
                        #'my/title-char-filter)))
    (setf (elfeed-meta entry :title) (funcall do-all-fixes title))))

(defun my/elfeed-fix-title-sel ()
  (interactive)
  (let ((entries (elfeed-search-selected)))
    (dolist (entry entries)
      (my/elfeed-fix-title entry)
      (elfeed-search-update-entry entry))))

(defun my/elfeed-hide-if-annoying (entry)
  (interactive)
  (let* ((title (elfeed-entry-title entry)))
    (when (s-contains? "advent " title t)
      (message "marking junk: %s" title)
      (elfeed-untag entry 'unread))))

(defadvice elfeed (after configure-elfeed activate)
  (add-hook 'elfeed-new-entry-hook #'my/elfeed-fix-title)
  (add-hook 'elfeed-new-entry-hook #'my/elfeed-hide-if-annoying))

;; YT bits

(defun elfeed-show-youtube-dl ()
  "Download the current entry with youtube-dl."
  (interactive)
  (pop-to-buffer (youtube-dl (elfeed-entry-link elfeed-show-entry))))

(defun elfeed-search-youtube-dl ()
  "Download the current entry with youtube-dl."
  (interactive)
  (let ((entries (elfeed-search-selected)))
    (dolist (entry entries)
      (if (null (youtube-dl (elfeed-entry-link entry)))
          (message "Entry is not a YouTube link!")
        (message "Downloading %s" (elfeed-entry-title entry)))
      (elfeed-untag entry 'unread)
      (elfeed-search-update-entry entry)
      (unless (use-region-p) (forward-line)))))

(defun my/yt-dlp-remote-dl ()
  (interactive)
  (let ((entries (elfeed-search-selected)))
    (dolist (entry entries)
      (let ((id (youtube-dl--id-from-url (elfeed-entry-link entry))))
        (if (null id)
            (message "Entry is not a YouTube link!")
          (progn
            (message "Downloading %s" (elfeed-entry-title entry))
            (start-process "ya" nil "ya" (elfeed-entry-link entry))
            (elfeed-untag entry 'unread)
            (elfeed-search-update-entry entry)
            (unless (use-region-p) (forward-line))))))))

(defun my/elfeed-search-youtube-dl ()
  (interactive)
  (if (f-exists-p "~/.bin/ya")
      (my/yt-dlp-remote-dl)
    (elfeed-search-youtube-dl)))

;; Keys

(define-key elfeed-search-mode-map "d" 'my/elfeed-search-youtube-dl)
(define-key elfeed-search-mode-map "t" 'my/elfeed-fix-title-sel)

(define-key elfeed-search-mode-map "h"
  (lambda ()
    (interactive)
    (elfeed-search-set-filter (my/elfeed-default-search))))

(define-key elfeed-search-mode-map "j"
  (lambda ()
    (interactive)
    (cl-macrolet ((re (re rep str) `(replace-regexp-in-string ,re ,rep ,str)))
      (elfeed-search-set-filter
       (cond
        ((string-match-p "-jp" elfeed-search-filter)
         (re " *-jp" " +jp" elfeed-search-filter))
        ((string-match-p "\\+jp" elfeed-search-filter)
         (re " *\\+jp" " -jp" elfeed-search-filter))
        ((concat elfeed-search-filter "-jp")))))))

(define-key elfeed-search-mode-map "u"
  (lambda ()
    (interactive)
    (cl-macrolet ((re (re rep str) `(replace-regexp-in-string ,re ,rep ,str)))
      (elfeed-search-set-filter
       (cond
        ((string-match-p "-unread" elfeed-search-filter)
         (re " *-unread" " " elfeed-search-filter))
        ((string-match-p "\\+unread" elfeed-search-filter)
         (re " *\\+unread" " " elfeed-search-filter))
        ((concat elfeed-search-filter "+unread")))))))

(define-key elfeed-show-mode-map "d" 'elfeed-show-youtube-dl)

(provide 'elfeed-settings)
