;;; -*- lexical-binding: t; -*-

(require 'calendar)
(require 'cl-lib)
(require 'dash)
(require 'f)
(require 's)
(require 'time-date)

;; Search string date range setup

(defun my/days-from-now-as-string (offset)
  (interactive)
  (let* ((date (calendar-current-date offset))
         (year (calendar-extract-year date))
         (month (calendar-extract-month date))
         (day (calendar-extract-day date)))
    (format "%04d-%02d-%02d" year month day)))

;; Morning newspaper simulation. By default, view yesterday's news, plus anything older-and-unread.
;; elfeed end date is UTC, non-inclusive.
;; See two days and older until 9:00 (local), one day and older after 9:00.
(defun my/elfeed-default-search ()
  (let* ((curr-hour (decoded-time-hour (decode-time)))
         (end-offset (if (< curr-hour 9) -1 0))
         (start-date (my/days-from-now-as-string -7))
         (end-date (my/days-from-now-as-string end-offset)))
    (format "@%s--%s +unread " start-date end-date)))

;; Title case

(defun my/first-case (word)
  (cond
   ((s-starts-with? "(" word)
    (concat "(" (s-capitalize (substring word 1))))
   ((s-starts-with? "'" word)
    (concat "'" (s-capitalize (substring word 1))))
   ((s-starts-with? "\"" word)
    (concat "\"" (s-capitalize (substring word 1))))
   (t
    (s-capitalize word))))

(defun my/rest-case (word)
  (cond
   ((with-temp-buffer
      (insert word)
      (goto-char (point-min))
      (> (count-matches "\\.") 1))
    word)
   ((-contains? '("a" "an" "as" "on" "and" "for"
                  "the" "of" "in" "to")
		(s-downcase word))
    (s-downcase word))
   (t (my/first-case word))))

(defun my/title-case (title)
  (interactive)
  (let* ((title-words (split-string title))
         (first-word (car title-words))
         (rest-words (cdr title-words)))
    (mapconcat 'identity (cons (my/first-case first-word) (mapcar 'my/rest-case rest-words)) " ")))

;; Character filters

(defun my/title-char-filter-single (char)
  (interactive)
  (let ((c (string char)))
    (cond
     ((-contains? '("“" "”") c)
      "\"")
     ((-contains? '("‘" "’") c)
      "'")
     (t c))))

(defun my/title-char-filter (word)
  (interactive)
  (mapconcat 'my/title-char-filter-single word ""))

(provide 'elfeed-utils)
