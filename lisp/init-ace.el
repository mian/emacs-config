;;; -*- lexical-binding: t -*-

(require 'use-package)

(use-package ace-window
  :bind
  ("C-x o" . ace-window)
  ("M-o" . ace-window)
  ("M-O" . ace-swap-window)
  :custom
  (aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)))

(provide 'init-ace)
