;;; -*- lexical-binding: t -*-

(require 'use-package)

(use-package savehist
  :ensure nil
  :config
  (savehist-mode))

(use-package recentf
  :demand t
  :ensure nil
  :custom
  (recentf-max-saved-items 50)
  (recentf-auto-cleanup 600) ; sec
  :config
  (add-to-list 'recentf-exclude #'file-remote-p)
  ;; Silence cleanup logging
  (defun my/mute-recentf-cleanup-messages (orig-fn &rest args)
    (let ((log-max message-log-max))
      (prog2
          (setq message-log-max nil
                inhibit-message t)
          (apply orig-fn args)
        (setq message-log-max log-max
              inhibit-message nil))))
  (advice-add 'recentf-cleanup :around #'my/mute-recentf-cleanup-messages)
  (recentf-mode 1))

(use-package vertico
  :demand t
  :preface
  (declare-function vertico-mode nil)
  :custom
  (enable-recursive-minibuffers t)
  :config
  (vertico-mode))

(use-package orderless
  :demand t
  :after vertico
  :custom (completion-styles '(orderless basic)))

(use-package marginalia
  :demand t
  :after orderless
  :preface
  (declare-function marginalia-mode nil)
  :config
  (marginalia-mode))

(use-package consult
  :defer 1
  :after marginalia
  :custom
  (consult-narrow-key (kbd "<"))
  (consult-widen-key (kbd ">"))
  :bind
  ;; ("C-s" . consult-isearch)
  ;; ("M-y" . consult-yank-pop)
  ("C-x b" . consult-buffer)
  ("C-c f" . consult-recent-file)
  ("C-c s" . consult-ripgrep))

(use-package corfu
  :defer 1
  :after consult
  :bind
  ("M-i" . completion-at-point)
  :config
  (global-corfu-mode))

(use-package embark
  :defer 1
  :after consult
  :bind
  ;; 💩
  ;; ("M-." . embark-dwim)
  ("C-." . embark-act))

(use-package sudo-edit
  :defer 1
  :after embark
  :config
  (define-key embark-file-map (kbd "S") 'sudo-edit-find-file))

(provide 'init-completion)
