(require 'use-package)

(use-package emacs
  :ensure nil
  :bind (("C-c v" . visual-line-mode)
         ("M-#" . dictionary-search)))

(use-package visual-fill-column
  :hook (visual-line-mode . visual-fill-column-mode))

(use-package adaptive-wrap
  :hook (visual-line-mode . adaptive-wrap-prefix-mode))

(use-package which-key
  :ensure nil
  :preface
  (declare-function which-key-mode nil)
  :delight
  :config
  (which-key-mode))

(use-package helpful
  :bind (("C-h h" . helpful-at-point)))

(use-package writeroom-mode
  :bind (("C-c w" . writeroom-mode))
  :hook (writeroom-mode . visual-line-mode))

(use-package whitespace-cleanup-mode
  :delight
  :hook
  (prog-mode . whitespace-cleanup-mode)
  (text-mode . whitespace-cleanup-mode))

(use-package autorevert
  :ensure nil
  :defer 1
  :config
  (global-auto-revert-mode))

(use-package drag-stuff
  :defer 2
  :preface
  (declare-function drag-stuff-define-keys nil)
  (declare-function drag-stuff-global-mode nil)
  :delight
  :config
  (drag-stuff-define-keys)
  (drag-stuff-global-mode 1))

(use-package highlight-indent-guides
  :defer 3
  :custom
  (highlight-indent-guides-method 'bitmap))

(use-package webpaste
  :defer 4
  :custom
  (webpaste-provider-priority '("ix.io" "paste.mozilla.org")))

(defun sort-symbols (reverse beg end)
  (interactive "*P\nr")
  (sort-regexp-fields reverse "\\(\\sw\\|\\s_\\)+" "\\&" beg end))

(defun undosify ()
  (interactive)
  (goto-char (point-min))
  (while (search-forward "\r" nil t) (replace-match "")))

(setq-default auto-save-default nil
              backup-directory-alist `(("." . ,(expand-file-name "backup/" user-emacs-directory)))
              default-input-method "japanese"
              inhibit-eol-conversion t
              isearch-lazy-count t
              locale-coding-system 'utf-8
              mouse-drag-and-drop-region-cross-program t
              mouse-drag-copy-region 'non-empty
              mouse-drag-mode-line-buffer t
              mouse-yank-at-point t
              save-interprogram-paste-before-kill t
              select-active-regions nil
              select-enable-clipboard t
              select-enable-primary t
              shift-select-mode nil)

(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

(set-default 'sentence-end-double-space nil)

(put 'erase-buffer 'disabled nil)

;; Keys
(keymap-global-set "<mouse-2>" 'mouse-yank-at-click)
(keymap-global-set "C-x C-k" 'kill-region)
(keymap-global-set "M-;" 'comment-or-uncomment-region)
(keymap-global-set "C-x k" 'kill-current-buffer)
(keymap-global-set "M-j" (lambda () (interactive) (join-line -1)))
(keymap-global-set "C-M-<backspace>" 'backward-kill-sexp)
(keymap-global-set "C-x s" 'save-buffer)

(provide 'init-edit-setup)
