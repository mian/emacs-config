;;; -*- lexical-binding: t -*-

(let ((value (concat "~/.bin:~/.cargo/bin:~/.nix-profile/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:" (getenv "PATH"))))
  (setenv "PATH" value)
  (defvar eshell-path-env)
  (setq-default eshell-path-env value
                exec-path (append (parse-colon-path value) (list exec-directory))))

(cd "~")

(provide 'init-exec-path)
