(require 'use-package)
(require 'elfeed-utils)
(require 'youtube-dl)

(use-package elfeed
  :if window-system
  :bind ("C-c n". elfeed)
  :custom
  (elfeed-search-filter (my/elfeed-default-search))
  (elfeed-search-title-max-width 110)
  (youtube-dl-directory (if (file-directory-p "~/Downloads") "~/Downloads" "~/downloads"))
  (youtube-dl-program "yt-dlp")
  (youtube-dl-arguments '())
  (url-queue-timeout 15)
  (url-queue-parallel-processes 1)
  :config
  (setq-default elfeed-log-level 'debug)
  (require 'elfeed-settings))

(use-package elfeed-org
  :demand t
  :after elfeed
  :preface
  (declare-function elfeed-org nil)
  :config
  (setq-default rmh-elfeed-org-files (list "~/org/feeds.org"))
  (elfeed-org))

(use-package elfeed-tube
  ;; breaking on aio.el: "Type aio-select missing from typeof-types!" as of 24.11
  :disabled t
  :demand t
  :after elfeed-org
  :custom
  (elfeed-tube-save-indicator nil)
  :config
  (elfeed-tube-setup))

(provide 'init-feeds)
