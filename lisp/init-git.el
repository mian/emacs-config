;;; -*- lexical-binding: t -*-

(require 'use-package)

(setq-default vc-handled-backends (quote (Git)))

(use-package magit
  :if window-system
  :bind ("C-c i" . magit-status)
  :functions magit-mode-quit-window
  :init
  (setq-default magit-auto-revert-mode nil)
  :custom
  ;; (magit-remote-git-executable "/home/ian/.nix-profile/bin/git") ;; XXX
  (magit-format-file-function #'magit-format-file-nerd-icons)
  (magit-merge-arguments '("--ff-only"))
  (magit-revision-headers-format "\
Author:     %aN
AuthorDate: %ad
Commit:     %cN
CommitDate: %cd
")
  (magit-log-revision-headers-format "\
%+b%+N
Author:    %aN
Committer: %cN"))

(use-package nerd-icons
  :if window-system
  :after magit
  :custom
  (nerd-icons-font-family "Inconsolata Nerd Font"))

(use-package fullframe
  :if window-system
  :after magit
  :config
  (fullframe magit-status magit-mode-quit-window))

(use-package forge
  ;; sqlite-builtin being dumb, not actually using so punt for now
  :disabled t
  :if window-system
  :after magit
  :custom
  (forge-database-connector 'sqlite-builtin))

(use-package git-link
  :if window-system
  :defer 10)

(use-package git-timemachine
  :if window-system
  :defer 10)

(provide 'init-git)
