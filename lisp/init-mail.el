(require 'use-package)

(defvar my/obfuscated-addr '("in.net" "an" "@ij" "i"))
(defun my/unshuffle (strs)
  (string-join (-sort (lambda (a b) (< (length a) (length b))) strs)))

(setenv "MAIL_ADDRESS" (my/unshuffle my/obfuscated-addr))

(use-package mu4e
  :ensure nil
  :if (display-graphic-p)
  :bind ("C-c m" . mu4e)
  :custom
  (user-full-name "M. Ian Graham")
  (user-mail-address (getenv "MAIL_ADDRESS"))

  (mu4e-get-mail-command (if (file-directory-p "/etc/profiles/per-user/ian/bin")
                             "/etc/profiles/per-user/ian/bin/mbsync -a"
                           "/Users/ian/.nix-profile/bin/mbsync -a"))
  (mu4e-update-interval 300)
  (mu4e-attachment-dir (if (file-directory-p "~/Downloads") "~/Downloads" "~/downloads"))

  (mu4e-drafts-folder "/Drafts")
  (mu4e-sent-folder "/Sent")
  (mu4e-trash-folder "/Trash")
  (mu4e-refile-folder "/Archive")
  (mu4e-maildir-shortcuts
   '(("/Inbox"  . ?i)
     ("/Sent"   . ?s)
     ("/Trash"  . ?t)
     ("/Archive"  . ?a)))
  (mu4e-bookmarks
   '(("maildir:/Inbox AND NOT flag:trashed" "Inbox" ?i)
     ("maildir:/Folders/bug-gnu-emacs AND NOT flag:trashed" "bug-gnu-emacs" ?b)))

  (mu4e-headers-show-threads nil)
  (mu4e-headers-include-related nil)
  (mu4e-headers-date-format "%Y/%m/%d")
  (mu4e-headers-time-format "%T")

  (mu4e-headers-fields '((:human-date . 12)
                         ;;(:flags . 6)
                         (:from . 22)
                         (:mailing-list . 10)
                         (:subject)))

  (mu4e-compose-signature nil)
  (mu4e-compose-signature-auto-include nil)

  (mu4e-change-filenames-when-moving t)
  (mu4e-completing-read-function 'completing-read)
  (mu4e-confirm-quit nil)
  (mu4e-hide-index-messages t)
  (mu4e-split-view nil)
  (mu4e-use-fancy-chars t)
  (mu4e-modeline-show-global nil)

  (message-kill-buffer-on-exit t))

;; smtpmail-mail-address (getenv "MAIL_ADDRESS")
;; smtpmail-smtp-server "smtp.googlemail.com"
;; smtpmail-default-smtp-server "smtp.googlemail.com"
;; smtpmail-stream-type 'starttls
;; smtpmail-smtp-service 587
;; smtpmail-smtp-user (getenv "MAIL_ADDRESS")
;; message-send-mail-function 'smtpmail-send-it

;;(run-with-idle-timer 600 t 'mu4e-update-mail-and-index t)

;;; WHYYYYYYYY
;;(after-load 'mu4e
;; (fullframe jump-to-mu4e-inbox mu4e~headers-quit-buffer)

;; (advice-add 'mu4e~headers-quit-buffer :after
;;             (lambda ()
;;               (run-at-time
;;                0.1 nil
;;                'bury-buffer)))

;; (defun jump-to-mu4e-inbox ()
;;   (interactive)
;;   (mu4e~headers-jump-to-maildir "/inbox"))
;; (global-set-key (kbd "C-c m") 'jump-to-mu4e-inbox)

;; (defun my-mu4e-mark-execute-all-no-confirm ()
;;   "Execute all marks without confirmation."
;;   (interactive)
;;   (mu4e-mark-execute-all 'no-confirm))

;; (define-key mu4e-headers-mode-map "x" #'my-mu4e-mark-execute-all-no-confirm)

;; (run-with-idle-timer
;;  600 t
;;  (lambda ()
;;    (delete-other-windows)
;;    (split-window-below)
;;    (jump-to-mu4e-inbox)))

;; (when window-system
;;   (mu4e~start))

(provide 'init-mail)
