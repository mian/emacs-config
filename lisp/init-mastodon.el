(use-package mastodon
  :disabled t
  :defer 2
  :custom
  (mastodon-instance-url "https://mstdn.jp")
  (mastodon-active-user "mian"))

(provide 'init-mastodon)
