(require 'use-package)

(use-package ement
  :demand
  :defer 2
  :config
  (require 'ement-room-list)
  (require 'ement-tabulated-room-list))


(provide 'init-matrix)
