;;; -*- lexical-binding: t -*-

(require 'use-package)

(defvar my/bibfiles '("~/org/bibliography/zotero.bib"))

(defun my/new-post-file ()
  (find-file (expand-file-name
              (format-time-string "%Y-%m-%d--%H-%M-%S.org")
              "~/ianworks/posts")))

(defun my/post-capture ()
  nil)

(use-package org
  :bind
  (("C-c a" . org-agenda)
   ("C-c c" . org-capture))

  :hook
  (elfeed-search-mode . org-load-modules-maybe)

  :defines
  (org-agenda-skip-scheduled-if-done
   org-agenda-span
   org-capture-templates)

  :functions
  (org-load-modules-maybe
   org-agenda-quit)

  :custom
  (org-modules '(org-agenda org-capture org-tempo ox-latex))
  (org-default-notes-file "~/org/notes.org")
  (org-agenda-files '("~/org/lift.org" "~/org/repeat.org" "~/org/todo.org"))
  (org-agenda-skip-scheduled-if-done t)
  (org-agenda-span 8)
  (org-catch-invisible-edits 'show)
  (org-confirm-babel-evaluate nil)
  (org-hide-emphasis-markers t)
  (org-hide-leading-stars t)
  (org-log-done 'time)
  (org-outline-path-complete-in-steps nil)
  (org-refile-targets '((org-agenda-files :maxlevel . 3)))
  (org-refile-use-outline-path 'file)
  (org-return-follows-link t)
  (org-src-preserve-indentation t)
  (org-src-tab-acts-natively t)
  (org-tags-column -120)
  (org-use-speed-commands t)
  (fill-column 100)
  (org-todo-keywords '((sequence "TODO" "DONE")))
  (org-capture-templates '(("t" "Task" entry
                            (file "~/org/todo.org")
                            "* TODO %^{Task}"
                            :immediate-finish t)
                           ("s" "Scheduled Task" entry
                            (file "~/org/todo.org")
                            "* TODO %^{Task}\n  SCHEDULED: %^t"
                            :immediate-finish t)
                           ("n" "Note" entry
                            (file "~/org/notes.org")
                            "* %^{Note}"
                            :immediate-finish t)
                           ("j" "Journal" entry
                            (file+olp+datetree "~/org/journal.org"))
                           ("p" "Post to Homepage" plain
                            (function my/new-post-file)
                            "#+TITLE: %^{PROMPT}\n#+DATE: %T\n#+MINI: t\n#+OPTIONS: toc:nil num:nil\n\n%?"
                            :kill-buffer t)))

  :init
  (setq-default org-babel-key-prefix "\C-cb")

  :config
  (org-load-modules-maybe 't))

(use-package fullframe
  :after org
  :config
  (fullframe org-agenda-list org-agenda-quit)
  (fullframe org-capture my/post-capture))

(use-package org-modern
  :after org
  :config
  (add-hook 'org-mode-hook
            (lambda ()
              (org-modern-mode 1)
              (org-indent-mode 1))))

(use-package pdf-tools
  :demand t
  :after org)

(use-package org-attach-screenshot
  :demand t
  :after org
  ;; :bind ("<f6> s" . org-attach-screenshot)
  :config
  (setopt org-attach-screenshot-dirfunction
          (lambda ()
	    (progn (cl-assert (buffer-file-name))
		   (concat (file-name-sans-extension (buffer-file-name))
			   "-att")))
          org-attach-screenshot-command-line "wl-paste > %f"))

(use-package denote
  :demand t
  :after org
  :custom
  (denote-directory "~/org/denote")
  (denote-known-keywords '("emacs" "org" "archive" "nix" "rust" "attention" "functionalprogramming" "gameprogramming" "math" "operations" "psychology" "science" "theory")))

(use-package moc
  :after org)

(use-package qrencode
  :after moc)

(let ((original-fg nil))
  (defun my/hide-begin-src ()
    (setq original-fg (face-attribute 'org-block-begin-line :foreground nil 'default))
    (set-face-attribute 'org-block-begin-line nil :foreground (face-attribute 'org-block-begin-line :background nil 'default)))
  (defun my/restore-begin-src ()
    (set-face-attribute 'org-block-begin-line nil :foreground original-fg)))

(use-package dslide
  :after qrencode
  :bind (("C-c d" . dslide-deck-start))
  :custom
  (dslide-breadcrumb-separator " . ")
  (dslide-feedback-messages nil)
  (dslide-start-hook (lambda ()
                       (dslide-cursor-hide)
                       (my/hide-begin-src)
                       (set-face-attribute 'default nil :height 320)))
  (dslide-stop-hook #'my/restore-begin-src)
  :config
  (keymap-set dslide-mode-map "q" #'dslide-deck-stop))

(provide 'init-org)
