;;; -*- lexical-binding: t -*-

(require 'use-package)
(require 'delight)
(require 'bind-key)

(use-package ppp
  :commands 'my/print-require-times
  :config
  (defun my/print-require-times ()
    (interactive)
    (ppp-alist (sort my/require-times (lambda (a b) (> (cdr a) (cdr b)))))))

(provide 'init-packages)
