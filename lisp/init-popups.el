;;; -*- lexical-binding: t -*-

(require 'use-package)

(use-package shackle
  :demand t
  :preface
  (declare-function shackle-mode nil)
  :delight
  :custom
  (shackle-rules '(("*Messages*" :select nil :other t :align bottom :size 0.2 :inhibit-window-quit nil)
                   ("\\*Messages\\*" :select nil :other t :align bottom :size 0.2 :inhibit-window-quit nil)
                   ("\\*Flymake diagnostics for .*\\*" :regexp t :select nil :other t :align bottom :size 0.2 :inhibit-window-quit nil)
                   ;; (".*magit.*" :regexp t :frame t :same t :select t)
		   ("*Buffer List*" :select t :same t)
                   ("*command-log*" :select nil :other t :align right :size 0.2 :inhibit-window-quit nil)
                   (geiser-debug-mode :select nil :popup t :align bottom :size 0.2)
                   ("*Geiser Guile REPL*" :select nil :popup t :align bottom :size 0.2)
                   ("*compilation*" :select nil :popup t :align bottom :size 0.3)
                   ))
  (shackle-default-rule nil)
  (shackle-inhibit-window-quit-on-same-windows t)
  :config
  (shackle-mode))

(use-package popper
  :demand t
  :after shackle
  :delight
  :bind (("C-`"   . popper-toggle-latest)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :custom
  (popper-mode-line " pop")
  (popper-reference-buffers
   '("*command-log*"
     flymake-diagnostics-buffer-mode
     flymake-project-diagnostics-mode
     geiser-debug-mode
     geiser-repl-mode
     compilation-mode))
  :config
  (popper-mode +1))
;; (require 'popper-echo)
;; (popper-echo-mode +1))

(provide 'init-popups)
