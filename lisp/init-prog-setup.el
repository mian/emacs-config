(require 'use-package)
(require 'tabulated-list)

(setq-default c-basic-indent 2
              c-basic-offset 2
              standard-indent 2
              default-tab-width 2
              indent-tabs-mode nil ;;t
              sh-indentation 2
              sh-basic-offset 2
              tabulated-list-use-header-line nil
              tags-file-name "TAGS"
              compilation-window-height 12)

;; (set-face-attribute 'tabulated-list-fake-header t :overline nil)

(use-package eglot
  :defer 1
  :bind
  ("C-c e" . (lambda () (interactive) (eglot-ensure)))
  ("M-n" . flymake-goto-next-error)
  ("M-p" . flymake-goto-prev-error)
  ("M-." . xref-find-definitions)
  :custom
  (max-mini-window-height 0.3)
  (eldoc-echo-area-display-truncation-message nil)

  :custom-face
  (tabulated-list-fake-header ((t (:overline nil :underline t :weight bold))))

  :config
  (defun my/eglot-hover-on ()
    (setq-local eldoc-documentation-functions '(eglot-signature-eldoc-function
                                                eglot-hover-eldoc-function)))

  (defun my/eglot-hover-off ()
    (setq-local eldoc-documentation-functions '(eglot-signature-eldoc-function)))

  (add-hook 'eglot-managed-mode-hook #'my/eglot-hover-off))

(use-package sideline
  :defer 1
  :hook (flymake-mode . sideline-mode)
  :init
  (require 'sideline-flymake)
  (setq-default sideline-flymake-display-errors-whole-line 'point
                sideline-backends-right '(sideline-flymake)))

(use-package direnv
  :unless (eq system-type 'darwin)
  :demand t
  :bind
  ("C-c d" . (lambda () (interactive)
               (direnv--disable)))
  :preface
  (declare-function direnv-mode nil)
  :custom
  (direnv-always-show-summary nil)
  :config
  (direnv-mode))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode-enable))

(use-package quickrun
  :defer 1
  :after direnv
  :bind
  ("C-c q" . (lambda () (interactive)
               (quickrun--init-command-key-table)
               (quickrun))))

(defvar my/diff-hl-bmp
  (define-fringe-bitmap 'mood-one-theme--diff-hl-bmp
    (vector #b00001000)
    1 8
    '(center t)))

(defun my/diff-hl-bmp-function (_type _pos)
  my/diff-hl-bmp)

(use-package diff-hl
  :disabled t
  :defer 1
  :hook (prog-mode . diff-hl-mode)
  :preface
  (declare-function global-diff-hl-mode nil)
  :config
  (setopt diff-hl-fringe-bmp-function #'my/diff-hl-bmp-function)
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh))

(use-package tempel
  :defer 1)

(provide 'init-prog-setup)
