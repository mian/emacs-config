(require 'use-package)

(defun goog(term)
  (interactive "sGoogle search: ")
  (browse-url
   (concat "http://www.google.com/search?q="
           (replace-regexp-in-string " " "+" term))))

(defun jsref(term)
  (interactive "sJavascript search: ")
  (browse-url
   (concat "http://www.google.com/search?btnI=I%27m+Feeling+Lucky&q=site%3Adeveloper.mozilla.org/en-US/docs/Web/JavaScript/Reference+"
           (replace-regexp-in-string " " "+" term))))

(defun haskref ()
  (interactive)
  (browse-url-generic (concat "https://hoogle.haskell.org/?hoogle=" (thing-at-point 'word))))

(keymap-global-set "C-c r g" 'goog)
(keymap-global-set "C-c r j" 'jsref)
(keymap-global-set "C-c r h" 'haskref)

(use-package define-word
  :bind ("C-c r d" . define-word-at-point))
(use-package wiki-summary
  :bind ("C-c r w" . wiki-summary))

(provide 'init-reference)
