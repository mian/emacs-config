;;; -*- lexical-binding: t -*-

;; Easy startup profiling.
;; Based on: https://github.com/purcell/emacs.d/blob/master/lisp/init-benchmarking.el

(defvar my/require-times nil)
(defvar my/init-profiling-done nil)

(defun my/time-elapsed-in-ms (end start)
  (round (* 1000.0 (float-time (time-subtract end start)))))

(defun my/build-require-times (orig-fn pkg &rest args)
  (let ((start-time (and (not (featurep pkg)) (current-time))))
    (prog1
        (apply orig-fn pkg args)
      (when (and (not my/init-profiling-done)
                 start-time
                 (featurep pkg))
        (add-to-list 'my/require-times
                     (cons pkg (my/time-elapsed-in-ms (current-time)
                                                      start-time)))))))

(advice-add 'require :around #'my/build-require-times)

(defun my/print-init-time ()
  (setq my/init-profiling-done t)
  (message "Startup done in %.2fms" (my/time-elapsed-in-ms after-init-time before-init-time)))

(add-hook 'after-init-hook #'my/print-init-time)

(provide 'init-startup-profile)
