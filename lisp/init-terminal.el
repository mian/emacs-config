(use-package eat
  :commands eat)

(use-package vterm
  :commands vterm
  :custom
  (vterm-min-window-width 20))

(use-package hide-mode-line
  :demand t
  :after vterm
  :config
  (add-hook 'vterm-mode-hook #'hide-mode-line-mode))

(provide 'init-terminal)
