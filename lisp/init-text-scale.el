;;; -*- lexical-binding: t -*-

(require 'transient) ; built in since v28

(defun my/text-scale-set (h)
  (set-face-attribute 'default nil :height h))

(defun my/text-scale-get ()
  (face-attribute 'default :height))

(defun my/text-scale-reset ()
  (interactive)
  (my/text-scale-set my/default-text-height))

(defun my/text-scale-bigger ()
  (interactive)
  (my/text-scale-set (+ (my/text-scale-get) 20)))

(defun my/text-scale-smaller ()
  (interactive)
  (my/text-scale-set (- (my/text-scale-get) 20)))

(transient-define-prefix my/text-scale-menu ()
  "Change scale of displayed text"
  :transient-suffix #'transient--do-stay
  :transient-non-suffix #'transient--do-warn
  ["Change text scale:"
   ("=" "bigger" my/text-scale-bigger)
   ("-" "smaller" my/text-scale-smaller)
   ("0" "reset" my/text-scale-reset)
   ("q" "quit" transient-quit-one)])

(dolist (k '("C-c 0"
             "C-c -"
             "C-c ="))
  (keymap-global-set k #'my/text-scale-menu))

(my/text-scale-reset)

;; Dumb defaults
(dolist (k '("C-x C-0"
             "C-x C--"
             "C-x C-="
             "C-x C-+"))
  (keymap-global-unset k 'remove))

(provide 'init-text-scale)
