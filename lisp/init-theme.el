;;; -*- lexical-binding: t -*-

(require 'use-package)

(defun my/setup-theme (theme-name)
  (when window-system
    (load-theme theme-name t)
    (fontaine-set-preset 'regular))

  (unless (display-graphic-p (selected-frame))
    (set-face-background 'default "unspecified-bg" (selected-frame))))

(defun my/setup-dark-theme ()
  (interactive)
  ;; Other good themes: catppuccin-theme, mood-one-theme, underwater-theme, jazz-theme, darktooth-theme
  (my/setup-theme 'doom-moonlight))

(defun my/setup-light-theme ()
  (interactive)
  (my/setup-theme 'doom-oksolar-light))


(use-package fontaine
  :defer 0
  :custom
  (fontaine-presets
   `((regular
      ;; Other good fonts: Recursive, Fantasque Sans Mono
      :default-family "Sarasa Mono J"
      :default-height ,my/default-text-height))))

(use-package doom-themes
  :after fontaine
  :defer 0
  :config
  (my/setup-dark-theme))

(use-package moody
  :if window-system
  :after doom-themes
  :custom
  (x-underline-at-descent-line t)
  :config
  (moody-replace-mode-line-buffer-identification)
  (moody-replace-vc-mode)
  (moody-replace-eldoc-minibuffer-message-function)
  (set-face-attribute 'mode-line          nil :box        nil)
  (set-face-attribute 'mode-line-inactive nil :box        nil))

(provide 'init-theme)
