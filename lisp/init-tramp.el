;;; -*- lexical-binding: t -*-

(require 'use-package)

(use-package tramp
  :ensure nil
  :defer 2
  :custom
  (tramp-default-method "ssh")
  ;; (tramp-remote-path '(tramp-default-remote-path "/bin" "/usr/bin" "/sbin" "/usr/sbin"
  ;;   "/usr/local/bin" "/usr/local/sbin" "/local/bin" "/local/freeware/bin"
  ;;   "/local/gnu/bin" "/usr/freeware/bin" "/usr/pkg/bin" "/usr/contrib/bin"
  ;;   "/opt/bin" "/opt/sbin" "/opt/local/bin" "/home/ian/.nix-profile/bin"))
  (vc-ignore-dir-regexp (format "\\(%s\\)\\|\\(%s\\)"
                                vc-ignore-dir-regexp
                                tramp-file-name-regexp)))

(provide 'init-tramp)
