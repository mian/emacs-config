(defvar my/treemacsfont "Fantasque Sans Mono")

(use-package treemacs
  :disabled t
  :bind ("C-c t" . treemacs)
  :custom
  (treemacs-width 30)
  :config
  (set-face-font 'treemacs-directory-face my/treemacsfont)
  (set-face-font 'treemacs-file-face my/treemacsfont)
  (set-face-font 'treemacs-fringe-indicator-face my/treemacsfont)
  (set-face-font 'treemacs-git-added-face my/treemacsfont)
  (set-face-font 'treemacs-git-conflict-face my/treemacsfont)
  (set-face-font 'treemacs-git-ignored-face my/treemacsfont)
  (set-face-font 'treemacs-git-modified-face my/treemacsfont)
  (set-face-font 'treemacs-git-renamed-face my/treemacsfont)
  (set-face-font 'treemacs-git-unmodified-face my/treemacsfont)
  (set-face-font 'treemacs-git-untracked-face my/treemacsfont)
  (set-face-font 'treemacs-help-column-face my/treemacsfont)
  (set-face-font 'treemacs-help-title-face my/treemacsfont)
  (set-face-font 'treemacs-on-failure-pulse-face my/treemacsfont)
  (set-face-font 'treemacs-on-success-pulse-face my/treemacsfont)
  (set-face-font 'treemacs-root-face my/treemacsfont)
  (set-face-font 'treemacs-root-remote-face my/treemacsfont)
  (set-face-font 'treemacs-root-remote-disconnected-face my/treemacsfont)
  (set-face-font 'treemacs-root-remote-unreadable-face my/treemacsfont)
  (set-face-font 'treemacs-root-unreadable-face my/treemacsfont)
  (set-face-font 'treemacs-tags-face my/treemacsfont)
  (set-face-font 'treemacs-term-node-face my/treemacsfont))

(use-package treemacs-magit
  :disabled t
  :after treemacs magit)

(provide 'init-treemacs)
