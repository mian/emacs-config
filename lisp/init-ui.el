;;; -*- lexical-binding: t -*-

(require 'use-package)

(setq-default cursor-type 'box
              visible-cursor nil
              inhibit-splash-screen t
              initial-major-mode 'fundamental-mode
              initial-scratch-message nil
              scroll-bar-width 0
              ring-bell-function 'ignore
              use-short-answers t
              use-dialog-box nil
              default-directory "~/"
              mouse-wheel-follow-mouse t
              mouse-wheel-progressive-speed nil
              fast-but-imprecise-scrolling t
              make-pointer-invisible nil
              browse-url-browser-function (if (string= system-type "darwin") 'browse-url-default-browser 'browse-url-firefox))

(blink-cursor-mode -1)

(when (eq system-type 'darwin)
  (setq-default frame-title-format nil
                frame-resize-pixelwise t
                ns-command-modifier 'meta
                ns-alternate-modifier 'meta
                ns-use-proxy-icon nil)
  (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t)))

;; Mute startup message
(defun display-startup-echo-area-message ()
  (message nil))

;; Early unbinding of dumb defaults
(dolist (k '("C-z"
             "C-c C-c"
             "C-x s"
             "M-;"
             "M-."
             "S-<backspace>"
             "<f10>"))
  (keymap-global-unset k 'remove))

(use-package init-text-scale
  :ensure nil
  :defer 1)

(use-package pulse
  :ensure nil
  :defer 1
  :if window-system
  :config
  (dolist (c '(scroll-up-command
               scroll-down-command
               recenter-top-bottom
               other-window))
    (advice-add c :after
                (lambda (&rest _)
                  (pulse-momentary-highlight-one-line (point))))))

(use-package zoom
  :bind
  ("C-c z" . zoom)
  ("C-c Z" . zoom-mode)
  :custom
  (zoom-size '(0.618 . 0.618)))

(use-package daemons
  :defer 2)

(provide 'init-ui)
