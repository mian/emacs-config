(use-package biome
  :commands 'biome
  :config
  (setq-default biome-query-coords
                '(("Tokyo, Japan" 35.70025 139.77933))))

(provide 'init-weather)
