(require 'use-package)

(use-package csharp-mode
  :ensure nil
  :mode ("\\.cs\\'" . csharp-mode))

(provide 'init-csharp)
