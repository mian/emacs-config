(require 'use-package)

(use-package fennel-mode
  :mode ("\\.fnl\\'" . fennel-mode))
;; :custom
;; (lua-indent-level 2))

(provide 'init-fennel)
