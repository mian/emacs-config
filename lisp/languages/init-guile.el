(require 'use-package)

(use-package geiser-guile
  :commands geiser)

(provide 'init-guile)
