(require 'use-package)

(defun haskell-mode-after-save-handler ())

(use-package haskell-mode
  :mode ("\\.hs\\'" . haskell-mode)
  :custom
  (haskell-mode-hook '(haskell-indentation-mode))
  :custom
  (haskell-process-type 'stack-ghci)
  (haskell-interactive-popup-errors nil))

(provide 'init-haskell)
