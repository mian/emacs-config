(require 'use-package)

(use-package js2-mode
  :mode ("\\.js\\'" . js2-mode)
  :custom
  (js-indent-level 2)
  (js2-mode-show-parse-errors nil)
  (js2-mode-show-strict-warnings nil))

(use-package json-mode
  :mode (("\\.json\\'" . json-mode)))

(provide 'init-javascript)
