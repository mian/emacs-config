(require 'use-package)

(use-package lua-mode
  :mode ("\\.lua\\'" . lua-mode)
  :custom
  (lua-indent-level 2))

(provide 'init-lua)
