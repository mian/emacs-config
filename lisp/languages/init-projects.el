;;; -*- lexical-binding: t -*-

(require 'use-package)

(use-package project
  :ensure nil
  :demand t
  :after marginalia
  :bind-keymap ("C-c p" . project-prefix-map))

(use-package xref
  :ensure nil
  :defer 1
  :custom
  (xref-search-program 'ripgrep)
  ;; xref has issues with unusual paths. Point it directly at the rg binary.
  (xref-search-program-alist
   (let* ((rgexec (executable-find "rg"))
          (rgcmd (format "xargs -0 %s <C> --null -nH --no-heading --no-messages -g '!*/' -e <R>" rgexec)))
     `((ripgrep . ,rgcmd)))))

(use-package bookmark
  :disabled t
  :ensure nil
  :defer 1
  :custom
  (bookmark-save-flag 1)
  :config
  (list-bookmarks))

(use-package burly
  :defer 1)

(use-package tldr
  :defer 3)

(provide 'init-projects)
