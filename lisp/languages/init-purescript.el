(require 'use-package)

(use-package purescript-mode
  :disabled t
  :mode ("\\.purs\\'" . purescript-mode)
  :functions turn-on-purescript-simple-indentation
  :preface
  (declare-function purescript-move-nested nil)
  :config
  (defun ps-hook ()
    (turn-on-purescript-simple-indentation)
    (define-key purescript-mode-map (kbd "C-<right>")
                (lambda ()
                  (interactive)
                  (purescript-move-nested 1)))
    (define-key purescript-mode-map (kbd "C-<left>")
                (lambda ()
                  (interactive)
                  (purescript-move-nested -1))))
  (add-hook 'purescript-mode-hook 'ps-hook))

(use-package psci
  :disabled t
  :after purescript-mode)

(provide 'init-purescript)
