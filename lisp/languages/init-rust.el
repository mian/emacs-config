(require 'use-package)

(use-package rust-mode
  :mode ("\\.rs\\'" . rust-mode)
  :bind ("C-c b" . rust-compile)
  :custom
  (compilation-always-kill t)
  (rust-indent-offset 2)
  (rust-format-on-save t)
  (rust-format-show-buffer nil)
  (rust-format-goto-problem nil))

(use-package ron-mode
  :mode ("\\.ron\\'" . ron-mode))

(use-package toml-mode
  :mode ("\\.toml\\'" . toml-mode))

(provide 'init-rust)
