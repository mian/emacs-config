(require 'use-package)

(use-package terraform-mode
  :mode (("\\.tf\\'" . terraform-mode))
  :config
  (add-hook 'terraform-mode-hook #'terraform-format-on-save-mode))

(provide 'init-terraform)
