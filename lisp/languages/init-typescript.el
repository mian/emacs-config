(require 'use-package)

(use-package typescript-mode
  :mode ("\\.ts\\'" . typescript-mode))

(use-package tide
  :after typescript-mode
  :preface
  (declare-function tide-setup nil)
  (declare-function tide-hl-identifier-mode nil)
  :custom
  (tide-tsserver-executable "node_modules/typescript/bin/tsserver")
  (tide-tsserver-process-environment '("TSS_LOG=-level verbose -file /tmp/tss.log"))
  (tide-format-options '(:InsertSpaceAfterFunctionKeywordForAnonymousFunctions nil :PlaceOpenBraceOnNewLineForFunctions nil :InsertSpaceAfterOpeningAndBeforeClosingNonemptyBrackets nil :InsertSpaceAfterOpeningAndBeforeClosingNonemptyBraces nil))
  :config
  (add-hook 'typescript-mode-hook (lambda ()
                                    (tide-setup)
                                    (eldoc-mode +1)
                                    (tide-hl-identifier-mode +1))))

(provide 'init-typescript)
